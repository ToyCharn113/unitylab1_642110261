using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class BasicMouseInformation : MonoBehaviour
{
    public Text m_TextMousePosition;
    public Text m_TextMouseScrolldelta;
    public Text m_TextMouseDeltaVector;
    public Text CurrentButton;
    Vector3 m_mousePreviousPosition;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mouseCurrentpos = Input.mousePosition;
        Vector3 mouseDeltaVector = Vector3.zero;
        mouseDeltaVector = (mouseCurrentpos - m_mousePreviousPosition).normalized;
        m_TextMousePosition.text = Input.mousePosition.ToString();
        m_TextMouseScrolldelta.text = Input.mouseScrollDelta.ToString();
        m_TextMouseDeltaVector.text = mouseDeltaVector.ToString();

       


        m_mousePreviousPosition = mouseCurrentpos;
    }
}

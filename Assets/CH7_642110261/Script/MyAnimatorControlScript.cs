using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAnimatorControlScript : MonoBehaviour
{
    protected Animator m_Animator;

   private static readonly int Punch = Animator.StringToHash("Punch");
   private static readonly int Dancing = Animator.StringToHash("Dancing");
   private static readonly int State = Animator.StringToHash("State");
    private static readonly int Turn = Animator.StringToHash("Turn");
    private static readonly int Forward = Animator.StringToHash("Forward");
    private static readonly int Jump = Animator.StringToHash("Blend");
    private static readonly int Jump2 = Animator.StringToHash("B2");

    private void Start()
 {
 m_Animator = GetComponent<Animator>();
 }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_Animator.SetTrigger("Jump");
            m_Animator.SetFloat(Jump, 0.25f);
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            m_Animator.SetTrigger("Jump");
            m_Animator.SetFloat(Jump, 0.50f);
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            m_Animator.SetTrigger("Jump");
            m_Animator.SetFloat(Jump, 0.75f);
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            m_Animator.SetTrigger("Jump");
            m_Animator.SetFloat(Jump, 1f);
        }
        
        if (Input.GetKeyDown(KeyCode.V))
        {
            m_Animator.SetFloat(Turn, 0.3f);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            m_Animator.SetFloat(Forward, 0.5f);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            m_Animator.SetTrigger("Mixamo");
            m_Animator.SetFloat(Jump, 0);
            m_Animator.SetFloat(Jump2, 0);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            m_Animator.SetTrigger("Mixamo");
            m_Animator.SetFloat(Jump, 1);
            m_Animator.SetFloat(Jump2, 0);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            m_Animator.SetTrigger("Mixamo");
            m_Animator.SetFloat(Jump, 0);
            m_Animator.SetFloat(Jump2, 1);
        }
    }
}

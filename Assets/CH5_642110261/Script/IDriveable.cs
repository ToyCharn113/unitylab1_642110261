using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDriveable 
{
    void Drive();
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMobilePhone 
{
    void MakeACall();
    void PrintBrandName();
}

public class AppleIPhone : IMobilePhone
{
    public void MakeACall()
    {
         //Perform phone calling process
 //It�s the same MakeACall but each brand has its own design of make a call process.
 }

 public void PrintBrandName()
    {
         Debug.Log("Apple iPhone");
         }
 }

 public class SamsungPhone : IMobilePhone
{
 public void MakeACall()
    {
         //Perform phone calling process
 //It�s the same MakeACall but each brand has its own design of make a call process.
 }

 public void PrintBrandName()
    {
         Debug.Log("Samsung");
         }
 }
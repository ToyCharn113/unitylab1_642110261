using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Chaicharn.GameDev3.Chapter5.InteracSystem
{
    public interface IActorEnterExitHandler
    {
        void ActorEnter(GameObject actor);
        void ActorExit(GameObject actor);
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chaicharn.GameDev3.Chapter1;
using Chaicharn.GameDev3.Chapter5.InteracSystem;
using Chaicharn.GameDev3.Chapter6.Inventory;



namespace Chaicharn.GameDev3.Chapter6.Interaction
{
    [RequireComponent(typeof(ItemTypeComponent))]

    public class Pickupable : MonoBehaviour, IInteractable,
    IActorEnterExitHandler
    {
        public void Interact(GameObject actor)
        {
            //myself
            var itemTypeComponent = GetComponent<ItemTypeComponent>();
            //actor
            var inventory = actor.GetComponent<Inventory2>();
            inventory.AddItem(itemTypeComponent.Type.ToString(), 1);

            Destroy(gameObject);
        }

        public void ActorEnter(GameObject actor)
        {

        }

        public void ActorExit(GameObject actor)
        {

        }
    }

}

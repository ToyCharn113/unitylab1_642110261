using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerGameObjectExample : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void OnTriggerEnter(Collider collider)
 {
 if (collider.gameObject.tag == "Item")
 {
 Debug.Log("Do something with Item");
 }

 if (collider.gameObject.tag == "Player")
 {
     Debug.Log("Do something with Player");
     }
 }

    // Update is called once per frame
    void Update()
    {
        
    }
}

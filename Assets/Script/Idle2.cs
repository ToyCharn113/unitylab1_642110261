using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using System;
using System.Reflection;
using Random = System.Random;


namespace Chaicharn.GameDev3.Chapter12
{
    public class Idle2 : MonoBehaviour
    {
        protected float m_IdleTime;


        public void EnterState()
        {
            RandomIdleTime();
        }

        private void RandomIdleTime()
        {
            m_IdleTime = UnityEngine.Random.Range(2, 5);
        }


        public void UpdateState()
        {
            ReduceIdleTimer();
        }


        private void ReduceIdleTimer()
        {
            m_IdleTime -= Time.deltaTime;
            if (m_IdleTime <= 0)
            {
                CustomEvent.Trigger(gameObject, "GotoWaypointReachingState");
            }
        }

        public void ExitState()
        {

        }
    }
}